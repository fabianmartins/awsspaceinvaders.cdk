# AWS Space Invaders CDK

**DISCLAIMER:** *AWS Spaceinvaders CDK, and all complementary resources are provided without any guarantees, and you're not recommended to use it for production-grade workloads. The intention is to providend content to build and learn.*

## What is this?

The AWS Space Invaders CDK is a fancy name for the Near Real-Time Applications (even fancier!) workshop back-end. This is an on-going development.


  message: 'User pool cannot be deleted. It has a domain configured that should be deleted first.',
  code: 'InvalidParameterException'



## How to prepare the environment

#### STEP 1 - Login to the AWS console
- Login to your account
- Select a region (take note of the region)
- **IMPORTANT:** Be sure that you have permissions to create resources on your account. For the purpose of this workshop, having administrative privileges is the best option.

#### STEP 2 - Launch your Cloud9 environment
1. On the AWS console, go to Cloud9. 
2. Launch Cloud9:
2.1. Go to the Cloud9 section of the console
2.2. Select `Create environment`
2.3. Give a name to your environment. **Important:** If you are sharing the same account and region with a colleague, be sure to take note of the identification of your environment, and be careful to not destroy your colleague environment.
2.4. For the "environment settings":
2.4.1. For "Environment type" choose `Create a new instance for environment (EC2)`
2.4.2. For "Instance type" choose `t2.micro (1 GiB RAM + 1 vCPU)*`
2.4.4. Leave the other configuration settings at their default values and click **Next step** and then **Create environment**
2.4.5. In a few seconds your environment will be available. You can clouse the Welcome tab.

#### STEP 3 - Updating your environment
1. Visit this page: https://docs.aws.amazon.com/CDK/latest/userguide/install_config.html. It indicates the requirements and steps to install CDK.
2. Check if the version of your *node* environment matches the minimum requirements specified at the CDk page. For my case, for example, the result was the following one:
```
~/environment $ node -v
v6.16.0
```
3. So, I needed to upgrade it, or at least to install a compatible version. At this time, the required version for Node.js is >= 8.11.x. To find the versions available run `nvm ls-remote` and, on the resulting list, search for the one indicated as *Latest LTS*. For my case, it was v10.15.2. So, I will install it:
```
~/environment $ nvm install --lts v10.15.2
Downloading https://nodejs.org/dist/v10.15.2/node-v10.15.2-linux-x64.tar.xz...
######################################################################################################################## 100.0%
Now using node v10.15.2 (npm v6.4.1)
~/environment $
```

#### STEP 4 - Installing CDK
Follow the steps indicated at https://docs.aws.amazon.com/CDK/latest/userguide/install_config.html, which possibly will be simply typing the command below
```
~/environment $ npm install -g aws-cdk
```

#### STEP 5 - Installing Typescript
```
 ~/environment $ npm install -g typescript
```

#### STEP 6 - Clone the repository and install the dependencies
1. Clone the repository
```
 ~/environment $ git clone https://fabianmartins@bitbucket.org/fabianmartins/awsspaceinvaders.cdk.git
```
2. (**IMPORTANT**) Get into the folder for the project
```
~/environment $ cd awsspaceinvaders.cdk
~/environment/awsspaceinvaders.cdk (master) $
```
3. Install the dependencies
```
~/environment/awsspaceinvaders.cdk (master) $ npm install
```

#### STEP 7 - Build the environment

You can build the environment by typing `npm run build`, but the best way of following up the progress of the project - particularly when you are doing changes - is by having 2 terminals open. One you will be using to issue commands. The other one, you will be using to monitor the progress of the development.

1. At the bottom of the page of your Cloud9 IDE, click the **`(+)`** icon to add a second terminal.
2. Go to the project folder
```
~/environment $ cd awsspaceinvaders.cdk/
```
3. Run the command `npm run watch` to watch for changes on your project and compile.
```
~/environment/awsspaceinvaders.cdk (master) $ npm run watch

> spaceinvaders.cdk@0.1.0 watch /home/ec2-user/environment/awsspaceinvaders.cdk
> tsc -w
[10:41:39 PM] Starting compilation in watch mode...

[10:41:52 PM] Found 0 errors. Watching for file changes.
```

#### STEP 8 : Deploy the environment

If your environment is ok, what can be identified by a message like this one on your watching window, then we are good to deploy:
```
[5:54:49 PM] Found 0 errors. Watching for file changes.
```

1. Explore the deployment by 

Y *"Useful commands"*

You are going to need:
* npm i @aws-cdk/aws-iam@0.22
* npm i @aws-cdk/aws-kinesis@0.22
* npm i @aws-cdk/aws-dynamodb@0.22
* npm i @aws-cdk/aws-ssm@0.22
* npm i @aws-cdk/aws-kinesisfirehose@0.22
* npm i @aws-cdk/aws-apigateway@0.22
* npm i @aws-cdk/aws-cognito@0.22
* npm i @aws-cdk/aws-cloudfront@0.22
* npm i @aws-cdk/aws-lambda-event-sources@0.22

# Useful commands

 * `npm run build`   compile typescript to js
 * `npm run watch`   watch for changes and compile
 * `cdk deploy`      deploy this stack to your default AWS account/region
 * `cdk diff`        compare deployed stack with current state
 * `cdk synth`       emits the synthesized CloudFormation template
